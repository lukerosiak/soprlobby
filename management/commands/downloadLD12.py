from soprlobby.models import *
from soprlobby.download import LD12Downloader

from django.core.management.base import NoArgsCommand

class Command(NoArgsCommand):
    help = "Get new quarterly lobbying disclosures."
    
    def handle_noargs(self, **options):
        
        downloader = LD12Downloader()
        downloader.go()
