from django.contrib import admin
from soprlobby.models import *


class ReportAdmin(admin.ModelAdmin):
    list_display = ('registrant', 'client', 'amount', 'year',)
    list_filter = ['period', 'year']
    
admin.site.register(Report, ReportAdmin)


