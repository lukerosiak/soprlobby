from django.db import models


class Report(models.Model):
    uniqID = models.CharField(max_length=36, primary_key=True)
    year = models.IntegerField(null=True)
    period = models.CharField(blank=True,max_length=150)
    typelong = models.CharField(blank=True,max_length=255)
    amount = models.IntegerField(null=True)
    
    registrant = models.CharField(blank=True,max_length=255)
    registrant_new = models.CharField(blank=True,max_length=255)
    registrantdescrip = models.TextField(blank=True)
    registrantid = models.IntegerField()
    registrantaddress = models.CharField(blank=True,max_length=255)
    registrantcountry = models.CharField(blank=True,max_length=255)
    registrantppb = models.CharField(blank=True,max_length=255)
    contact = models.CharField(blank=True,max_length=150)
   
    client = models.CharField(blank=True,max_length=255)
    client_new = models.CharField(blank=True,max_length=255)
    clientdescrip = models.TextField(blank=True)
    clientid = models.IntegerField()
    clientstatus = models.IntegerField()
    stateorlocalgov = models.IntegerField(null=True)
    clientstate = models.CharField(blank=True,max_length=150)
    clientppbstate = models.CharField(blank=True,max_length=150)
    clientppbcountry = models.CharField(blank=True,max_length=150)
    
    ultorg = models.CharField(blank=True,max_length=255)
    catcode = models.CharField(blank=True,max_length=5)
    use = models.CharField(blank=True,max_length=1)
    self = models.CharField(blank=True,max_length=1)

    received = models.DateTimeField()
    affiliateurl = models.CharField(blank=True,max_length=255)


class Lobbyist(models.Model):
    report = models.ForeignKey('Report')
    name = models.CharField(max_length=150)
    status = models.CharField(max_length=3)
    indicator = models.CharField(max_length=3)
    officialposition = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.name
    
class Issue(models.Model):
    report = models.ForeignKey('Report')
    issue = models.CharField(max_length=100)
    specificissue = models.TextField(blank=True)
    
    def __unicode__(self):
        return self.specificissue

class Agency(models.Model):
    report = models.ForeignKey('Report')
    agency = models.CharField(max_length=100)
    
    def __unicode__(self):
        return self.agency
    
class Affiliate(models.Model):
    report = models.ForeignKey('Report')
    name = models.CharField(max_length=255)
    country = models.CharField(max_length=100)
    ppb = models.CharField(max_length=100)
    
class ForeignOwner(models.Model):
    report = models.ForeignKey('Report')
    name = models.CharField(max_length=255)
    country = models.CharField(max_length=100)
    ppb = models.CharField(max_length=100)    
    contribution = models.IntegerField(null=True)
    percent = models.IntegerField(null=True)
    status = models.CharField(max_length=100)
    

