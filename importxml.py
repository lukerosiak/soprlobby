from datetime import datetime
from BeautifulSoup import BeautifulStoneSoup
from models import *


class ImportLD12xml:

    def __init__(self, filename):
        f = open(filename, 'r')
        self.soup = BeautifulStoneSoup(f)
 
    def parse(self):
        
        for f in self.soup.publicfilings.findAll('filing'):
            uniqID = f['id'] 
            year = int(f['year']) 
            
            lobbying = {}
            lobbying['uniqID'] = uniqID
            lobbying['year'] = year
            lobbying['typelong'] = f['type']
            lobbying['period'] = f['period']
            if f['amount']:
                lobbying['amount'] = int(f['amount'])
            timestamp = None
            try:
                timestamp = datetime.strptime(f['received'],"%Y-%m-%dT%H:%M:%S.%f")
            except:
                timestamp = datetime.strptime(f['received'],"%Y-%m-%dT%H:%M:%S")
            lobbying['received'] = timestamp
        
            lobbying['registrantid'] = int(f.registrant['registrantid'])
            lobbying['registrant'] = f.registrant['registrantname']
            lobbying['registrantdescrip'] = registrantdescrip = f.registrant['generaldescription']
            if 'address' in f.registrant:
                lobbying['registrantaddress'] = registrantaddress = f.registrant['address']
            lobbying['registrantcountry'] = f.registrant['registrantcountry']
            lobbying['registrantppb'] = f.registrant['registrantppbcountry']
    
            lobbying['client'] = f.client['clientname']
            lobbying['clientdescrip'] = f.client['generaldescription']
            lobbying['clientid'] = int(f.client['clientid'])
            lobbying['clientstatus'] = f.client['clientstatus']
            if 'contactfullname' in f.client:
                lobbying['contact'] = f.client['contactfullname'] 
            if 'isstateorlocalgov' in f.client:
                lobbying['stateorlocalgov'] = f.client['isstateorlocalgov'] 
            lobbying['clientppbcountry'] = f.client['clientcountry'] 
            lobbying['clientstate'] = f.client['clientstate'] 
            lobbying['clientppbstate'] = f.client['clientppbstate']
            
        
        
            report = Report(**lobbying)
            
            if 'affiliatedorgsurl' in f:
                report.affiliateurl = f['affiliatedorgsurl'] 
                
            report.save()
        
            if f.lobbyists:
                for l in f.lobbyists.findAll('lobbyist'):
                    lobbyist = {}
                    lobbyist['name'] = l['lobbyistname'].strip()
                    lobbyist['status'] = l['lobbyiststatus']
                    lobbyist['indicator'] = l['lobbyisteindicator']
                    if 'officialposition' in lobbyist:
                        lobbyist['officialposition'] = l['officialposition']
                    
                    report.lobbyist_set.create(**lobbyist)
        
            if f.governmententities:
                for g in f.governmententities.findAll('governmententity'):
                    report.agency_set.create( agency=g['goventityname'] )
        
            if f.issues:
                for i in f.issues.findAll('issue'):
                    issue = {}
                    issue['issue'] = i['code']
                    if 'specificissue' in issue:
                        issue['specificissue'] = i['specificissue']
                    report.issue_set.create(**issue)
            
            if f.foreignentities:
                for e in f.foreignentities.findAll('entity'):
                    foreign = {}
                    if 'foreignentitycontribution' in e:
                        foreign['contribution'] = e['foreignentitycontribution']
                    foreign['country'] = e['foreignentitycountry']
                    foreign['name'] = e['foreignentityname']
                    foreign['status'] = e['foreignentitystatus']
                    if 'foreignentityownershippercentage' in e:
                        foreign['percent'] = e['foreignentityownershippercentage']
                    foreign['ppb'] = e['foreignentityppbcountry']
                    report.foreignowner_set.create(**foreign)
            
            if f.affiliatedorgs:
                for a in f.affiliatedorgs.findAll('org'):
                    aff = {}
                    aff['country'] = a['affiliatedorgcountry']
                    aff['name'] = a['affiliatedorgname']
                    aff['ppb'] = a['affiliatedorgppbccountry']
                    report.affiliate_set.create(**aff)
    
            print 'done'
